package ru.t1.shipilov.tm.repository.model;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.shipilov.tm.api.repository.model.ISessionRepository;
import ru.t1.shipilov.tm.api.service.IConnectionService;
import ru.t1.shipilov.tm.api.service.IPropertyService;
import ru.t1.shipilov.tm.api.service.model.IUserService;
import ru.t1.shipilov.tm.enumerated.Role;
import ru.t1.shipilov.tm.marker.UnitCategory;
import ru.t1.shipilov.tm.migration.AbstractSchemeTest;
import ru.t1.shipilov.tm.model.Session;
import ru.t1.shipilov.tm.model.User;
import ru.t1.shipilov.tm.service.ConnectionService;
import ru.t1.shipilov.tm.service.PropertyService;
import ru.t1.shipilov.tm.service.model.UserService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static ru.t1.shipilov.tm.constant.SessionConstant.INIT_COUNT_SESSIONS;

@Category(UnitCategory.class)
public class SessionRepositoryTest extends AbstractSchemeTest {

    @NotNull
    private ISessionRepository repository;

    @NotNull
    private List<Session> sessionList;

    @Nullable
    private List<User> userList;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserService userService = new UserService(propertyService, connectionService);

    public static User USER;

    public static long USER_ID_COUNTER;

    public static EntityManager entityManager;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        entityManager = connectionService.getEntityManager();
    }

    @Before
    public void init() {
        repository = new SessionRepository(entityManager);
        sessionList = new ArrayList<>();
        userList = new ArrayList<>();
        entityManager.getTransaction().begin();
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++) {
            USER_ID_COUNTER++;
            USER = userService.create("session_rep_usr_" + USER_ID_COUNTER, "1");
            userList.add(USER);
        }
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++) {
            @NotNull final Session session = new Session();
            session.setUser(userList.get(i));
            session.setRole(Role.USUAL);
            repository.add(userList.get(i).getId(), session);
            sessionList.add(session);
        }
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
    }

    @After
    public void ClearAfter() {
        entityManager.getTransaction().commit();
        userService.clear();
    }

    @AfterClass
    public static void closeConnection() {
        entityManager.close();
    }

    @Test
    public void testAddSessionPositive() {
        @NotNull Session session = new Session();
        session.setUser(userList.get(0));
        session.setRole(Role.USUAL);
        repository.add(userList.get(0).getId(), session);
        Assert.assertEquals(2, repository.getSize(userList.get(0).getId()));
    }

    @Test
    public void testClear() {
        for (@NotNull final User user : userList) {
            Assert.assertEquals(1, repository.getSize(user.getId()));
            repository.clear(user.getId());
            Assert.assertEquals(0, repository.getSize(user.getId()));
        }
    }

    @Test
    public void testFindById() {
        for (@NotNull final User user : userList) {
            Assert.assertNull(repository.findOneById(user.getId(), UUID.randomUUID().toString()));
        }
        for (@NotNull final Session session : sessionList) {
            final Session foundSession = repository.findOneById(session.getUser().getId(), session.getId());
            Assert.assertNotNull(foundSession);
            Assert.assertEquals(session.getId(), foundSession.getId());
        }
    }

    @Test
    public void testExistsById() {
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        for (@NotNull final Session session : sessionList) {
            Assert.assertTrue(repository.existsById(session.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        for (@NotNull final Session session : sessionList) {
            Assert.assertNull(repository.findOneByIndex(session.getUser().getId(), 9999));
            final Session foundSession = repository.findOneByIndex(session.getUser().getId(), 1);
            Assert.assertNotNull(foundSession);
            Assert.assertEquals(session.getId(), foundSession.getId());
        }
    }

    @Test
    public void testFindAll() {
        for (@NotNull final User user : userList) {
            List<Session> sessions = repository.findAll(user.getId());
            Assert.assertNotNull(sessions);
            Assert.assertEquals(1, sessions.size());
            for (@NotNull final Session session : sessionList) {
                if (session.getUser().getId().equals(user.getId()))
                    Assert.assertNotNull(
                            sessions.stream()
                                    .filter(m -> session.getId().equals(m.getId()))
                                    .filter(m -> session.getUser().getId().equals(m.getUser().getId()))
                                    .findFirst()
                                    .orElse(null));
            }
        }
    }

    @Test
    public void testRemoveById() {
        for (@NotNull final Session session : sessionList) {
            repository.removeById(session.getUser().getId(), session.getId());
            Assert.assertNull(repository.findOneById(session.getUser().getId(), session.getId()));
            Assert.assertEquals(0, repository.getSize(session.getUser().getId()));
        }
    }

    @Test
    public void testRemove() {
        for (final Session session : sessionList) {
            Assert.assertEquals(1, repository.getSize(session.getUser().getId()));
            repository.remove(session.getUser().getId(), session);
            Assert.assertEquals(0, repository.getSize(session.getUser().getId()));
        }
    }

    @Test
    public void testRemoveWOUserId() {
        for (final Session session : sessionList) {
            Assert.assertEquals(1, repository.getSize(session.getUser().getId()));
            repository.remove(session);
            Assert.assertEquals(0, repository.getSize(session.getUser().getId()));
        }
    }

}

