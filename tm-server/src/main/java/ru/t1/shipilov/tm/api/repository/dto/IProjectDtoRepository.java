package ru.t1.shipilov.tm.api.repository.dto;

import ru.t1.shipilov.tm.dto.model.ProjectDTO;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDTO> {
}
