package ru.t1.shipilov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.api.repository.model.IProjectRepository;
import ru.t1.shipilov.tm.model.Project;

import javax.persistence.EntityManager;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected Class<Project> getEntityClass() {
        return Project.class;
    }

}

