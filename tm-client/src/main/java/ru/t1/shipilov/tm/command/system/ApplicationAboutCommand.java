package ru.t1.shipilov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.shipilov.tm.dto.response.ApplicationAboutResponse;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private final String ARGUMENT = "-a";

    @NotNull
    private final String NAME = "about";

    @NotNull
    private final String DESCRIPTION = "Show developer info.";

    @Override
    public void execute() {
        @NotNull final ApplicationAboutRequest request = new ApplicationAboutRequest();
        @NotNull final ApplicationAboutResponse response = getSystemEndpoint().getAbout(request);

        System.out.println("[APPLICATION]");
        System.out.println("NAME: " + response.getApplicationName());
        System.out.println();

        System.out.println("[ABOUT]");
        System.out.println("AUTHOR: " + response.getAuthorName());
        System.out.println("E-MAIL: " + response.getAuthorEmail());
        System.out.println();
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
