package ru.t1.shipilov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.api.model.ICommand;
import ru.t1.shipilov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    private final String ARGUMENT = "-h";

    @NotNull
    private final String NAME = "help";

    @NotNull
    private final String DESCRIPTION = "Show command list.";

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@NotNull final ICommand command: commands)  System.out.println(command);
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
