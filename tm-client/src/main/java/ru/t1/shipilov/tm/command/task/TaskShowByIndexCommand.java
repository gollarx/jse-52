package ru.t1.shipilov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.dto.request.TaskShowByIndexRequest;
import ru.t1.shipilov.tm.dto.model.TaskDTO;
import ru.t1.shipilov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-show-by-index";

    @NotNull
    private final String DESCRIPTION = "Show task by index.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() -1;
        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest(getToken());
        request.setIndex(index);
        @Nullable final TaskDTO task = getTaskEndpoint().showTaskByIndex(request).getTask();
        showTask(task);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
