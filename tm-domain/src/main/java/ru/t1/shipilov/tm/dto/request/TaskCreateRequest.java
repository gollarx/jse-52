package ru.t1.shipilov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskCreateRequest extends AbstractUserRequest {

    private String name;

    private String description;

    public TaskCreateRequest(@Nullable final String token) {
        super(token);
    }

}
